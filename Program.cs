﻿using System;
using System.Collections.Generic;
namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            Console.WriteLine("Ex 23 - Part A - Dictionary");
            
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add("Blade", "Action");
            d.Add("The Ring", "Horror");
            d.Add("The Exorcist", "Horror");
            d.Add("Die Hard", "Action");
            d.Add("John Carter", "Sci-Fi");

            foreach (KeyValuePair<string, string> entry in d)
            {
                Console.WriteLine($"{entry.Key} is a {entry.Value} movie");
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 23 - Part B - Print action movies");
            foreach (KeyValuePair<string, string> entry in d)
            {
                if (entry.Value == "Action")
                {
                    Console.WriteLine($"{entry.Key}");
                }
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 23 - Part C & D - Count horror movies - Put them in a list - print them");
            int count = 0;
            List<string> horrorMoves = new List<string>();
            foreach (KeyValuePair<string, string> entry in d)
            {
                if (entry.Value == "Horror")
                {
                    horrorMoves.Add(entry.Key);
                    count++;
                }
            }
            Console.WriteLine($"There are {count} horror movies");

            Console.WriteLine("The names of the horror movies are");
            for (int i = 0; i < horrorMoves.Count; i++)
            {
                Console.WriteLine(horrorMoves[i]);
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
